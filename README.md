﻿**Procesamiento de Imágenes**

**Trabajo Final**

**Sección:** CC62

**Grupo:** 4

**Docente:** Canaval Sánchez, Luis Martin

|N°|Estudiante|Código|
| :-: | :-: | :-: |
|1|Mendoza Manrique, Emily|U202122644|
|2|Pastor Salazar, Tomas Alonso|U201916314|
|3|Samaniego Millan, Ayrton Jafet|U202122788|










Lima, junio del 2024

**Índice** 

[**Introducción:**](#_intro)

[**Objetivos:**](#_objs)

[**Desarrollo:**](#_desa)

[**Conclusiones:**](#_conc)

[**Referencias Bibliográficas:**](#_ref)
#






































# <a name="_intro"></a></a>**Introducción:**
La organización mundial de la salud (2022) estima que para el 2050 habrá 3.5 billones de personas con discapacidades que necesitaran de productos asistivos para poder mejorar su calidad de vida. Estos pueden variar desde sillas de ruedas hasta softwares o aplicativos que se pueden desplegar desde un celular (WHO, 2022). Con el avance de nuevas tecnologías como la inteligencia artificial y la visión computacional se busca implementar nuevos productos asistivos que ayuden a personas con discapacidades y los hagan más accesibles (Weitzman, 2024).

En el futuro veremos esta tecnología usada diariamente, especialmente por personas con discapacidades para poder identificar objetos, personas o usando interfaces controladas a través de gestos (Sahoo & Choudhury, 2024). Sin embargo, la O.M.S. (2022) señala que el mayor problema con esta tecnología es su falta de accesibilidad debido a sus altos costos. Es por esto que es fundamental resolver esta problemática de manera costo efectiva y simple de usar.

En nuestro trabajo buscaremos solucionar esta problemática al implementar el uso librerias de Machine Learning combinadas con visión computacional para poder manejar una interfaz a través de gestos de mano de una manera rápida y costo eficaz. En este caso aplicaremos esta tecnología para poder jugar un juego de internet llamado “Watergirl & Fireboy”, que requiere que se utilice el teclado para poder jugar, pero el potencial del proyecto no se limita solo a esto, pues cada gesto puede ser un input que se declara dentro del código.

# <a name="_objs"></a>**Objetivos:**
El objetivo de nuestro trabajo es poder usar diferentes técnicas de procesamiento de imágenes y visión computacional para crear una aplicación que nos permita al usuario interactuar con una computadora solo con gestos de las manos. Para este proyecto vamos a implementar estos gestos únicamente para que sea posible jugar “Watergirl & Fireboy”.
# <a name="_desa"></a>**Desarrollo:**
Primero, debemos establecer las técnicas que utilizaremos para desarrollar el trabajo final. En este caso son 4 técnicas desarrollado: 

1. Conversion de color
1. Detección de manos
1. Dibujo de landmarks
1. Reconocimiento de gestos

Empezamos importando las librerías de “pyautogui” y “mediapipe”, la primera nos ayuda a controlar el mouse y teclado sin tener contacto directo, mientras que la segunda librería es un framework de Google que nos permite usar Machine Learning para crear modelos. Uno de los beneficios de “mediapipe” es que cuenta con una solución que nos permite detectar y seguir a las manos dentro de un video, específicamente, detecta las palmas de las manos y también puede dibujar las landmarks en tiempo real. Los susodichos landmarks son las articulaciones de las manos y permite que se puedan identificar gestos. Por último, utilizamos la librería de cv2 para poder procesar imágenes y videos. Cabe aclarar que el procesamiento de video va a ser en tiempo real.



Para empezar abrimos la cámara de la laptop y usamos el modelo de detección de manos y dibujo de landmarks de mediapipe. Después creamos un bucle while para poder capturar cada frame del video y convertimos cada frame de color BGR a RGB, mientras las imagenes se mantienen iguales lo que cambia es el canal de color principal. Segundo debemos identificar las manos en la imagen y si es derecha e izquierda, esto es importante debido a que se controla diferentes personajes con cada mano. La mano derecha controla a FireBoy, presionando las flechas direccionales para moverse mientras que la mano izquierda presiona las clásicas “WASD”, pero la S no se usa por la naturaleza del juego, controlando a WaterGirl.

Finalmente, reconocemos los gestos de las manos para poder especificar qué tecla debe ser presionada. Para poder moverse a la derecha el modelo debe identificar el meñique como el único dedo arriba y para la izquierda debe ser el índice. Para poder moverse a la derecha y saltar se debe identificar el índice y el pulgar como los únicos dedos arriba. Mientras que para moverse a la izquierda y saltar se debe visualizar arriba el índice y el meñique. Para poder reconocer estos gestos se utilizó los landmarks de los dedos en donde se obtiene la posición y de los dedos y se compara con las de otros dedos. De esta forma se pueden identificar los gestos y mover los personajes en el juego.
# <a name="_conc"></a>**Conclusiones:**
En conclusión, hemos podido cumplir con nuestro objetivo de crear una aplicación que permita al usuario jugar “Watergirl & Fireboy” utilizando gestos de mano. Esto se logró con las librerías de mediapipe, que usamos debido a que cuenta con un modelo de detección de manos y dibujo de landmarks. Basados en esos modelos pudimos reconocer los gestos que se hacían al verificar los dedos de las manos que se encontraban arriba.

Enlace del video demostrativo: <https://youtu.be/iMkCdxHfflU>
# <a name="_ref"></a>**Referencias Bibliográficas:**
Sahoo, S., & Choudhury, B. (2024). Exploring the use of computer vision in assistive technologies for individuals with disabilities: A review. *Journal of Future Sustainability*, 4(3), 133-148 <http://dx.doi.org/10.5267/j.jfs.2024.7.002>

Weitzman, T. (2024, 20 febrero). Empowering individuals with disabilities through AI technology. *Forbes*. <https://www.forbes.com/sites/forbesbusinesscouncil/2023/06/16/empowering-individuals-with-disabilities-through-ai-technology/> 

World Health Organization: WHO. (2022, 16 mayo). Almost one billion children and adults with disabilities and older persons in need of assistive technology denied access, according to new report. *World Health Organization*. <https://www.who.int/news/item/16-05-2022-almost-one-billion-children-and-adults-with-disabilities-and-older-persons-in-need-of-assistive-technology-denied-access--according-to-new-report>
