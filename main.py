import cv2
import pyautogui
import mediapipe as mp
import time


def Right_Left_Up(frame,x,y,pos):
    pyautogui.keyDown(x)
    pyautogui.keyDown(y)
    time.sleep(0.5)
    pyautogui.keyUp(x)
    pyautogui.keyUp(y)
    cv2.putText(frame, f"{x}{y}", (pos, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)

def Right_Left(frame,x,pos):
    pyautogui.keyDown(x)
    cv2.putText(frame, x, (pos, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
    
    
cap = cv2.VideoCapture(0)

mp_hands = mp.solutions.hands
hands = mp_hands.Hands(static_image_mode=False, max_num_hands=2,
                       min_detection_confidence=0.5, min_tracking_confidence=0.5)

mp_drawing = mp.solutions.drawing_utils

while True:
    ret, frame = cap.read()
    if not ret:
        break
    
    image_rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    
    results = hands.process(image_rgb)
    
    if results.multi_hand_landmarks:  
        for hand_id,hand_landmarks in enumerate(results.multi_hand_landmarks):
            lbl= results.multi_handedness[hand_id].classification[0].label
            mp_drawing.draw_landmarks(frame, hand_landmarks, mp_hands.HAND_CONNECTIONS)
            if(lbl == "Right"):
                handLandmarksL = []

                for landmarks in hand_landmarks.landmark:
                    handLandmarksL.append([landmarks.x,landmarks.y])

                if (handLandmarksL[8][1] < handLandmarksL[6][1] and handLandmarksL[20][1] < handLandmarksL[18][1]): #Index finger and Pinky
                    Right_Left_Up(frame,'left','up',10)                                                                                
                elif (handLandmarksL[8][1] < handLandmarksL[6][1] and handLandmarksL[4][1] < handLandmarksL[5][1]): #Index finger and Thumb  
                    Right_Left_Up(frame,'right','up',10)                                                                               
                elif handLandmarksL[8][1] < handLandmarksL[6][1]:     #Index
                    Right_Left(frame,'right',10)                                                                                         
                elif handLandmarksL[20][1] < handLandmarksL[18][1]:     #Pinky
                    Right_Left(frame,'left',10)
                elif (handLandmarksL[8][1]>handLandmarksL[4][1] and handLandmarksL[20][1]>handLandmarksL[4][1] and handLandmarksL[16][1]>handLandmarksL[4][1]  and handLandmarksL[12][1]>handLandmarksL[4][1] ):
                    pyautogui.keyUp('left')
                    pyautogui.keyUp('right')
                    cv2.putText(frame, "Left Hand", (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
                    
            elif (lbl == "Left"):
                
                handLandmarksR = []

                for landmarks in hand_landmarks.landmark:
                    handLandmarksR.append([landmarks.x,landmarks.y])

                if (handLandmarksR[8][1] < handLandmarksR[6][1] and handLandmarksR[20][1] < handLandmarksR[18][1]): #Index finger and Pinky
                    Right_Left_Up(frame,'A','W',580)
                elif (handLandmarksR[8][1] < handLandmarksR[6][1] and handLandmarksR[4][1] < handLandmarksR[5][1]): #Index finger and Thumb 
                    Right_Left_Up(frame,'D','W',580)
                elif handLandmarksR[8][1] < handLandmarksR[6][1]: #Index
                    Right_Left(frame,'D',600)
                elif handLandmarksR[20][1] < handLandmarksR[18][1]: #Pinky
                    Right_Left(frame,'A',600)
                elif (handLandmarksR[8][1]>handLandmarksR[4][1] and handLandmarksR[20][1]>handLandmarksR[4][1] and handLandmarksR[16][1]>handLandmarksR[4][1]  and handLandmarksR[12][1]>handLandmarksR[4][1] ):
                    pyautogui.keyUp('A')
                    pyautogui.keyUp('D')
                    cv2.putText(frame, "Right Hand", (450, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
                
    cv2.imshow('CV', frame)
    
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
    
cap.release()
cv2.destroyAllWindows()
